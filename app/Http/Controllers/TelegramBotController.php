<?php

namespace App\Http\Controllers;

use App\States\State;
use App\User;
use Illuminate\Http\Request;
use App\Telegram\TelegramRequest;
use Illuminate\Support\Facades\Auth;

class TelegramBotController extends Controller
{
    /** @var TelegramRequest $telegramRequest */
    private $telegramRequest;
    /** @var User $user */
    private $user;
    /** @var State $state */
    private $state;

    public function handle(Request $request)
    {
        $this->prepareRequest($request);
        $this->prepareUser();
        State::handleState($this->telegramRequest);
    }

    /**
     * @param Request $request
     */
    private function prepareRequest(Request $request)
    {
        $this->telegramRequest = new TelegramRequest();

        if(isset($request['message']))
        {
            $this->telegramRequest->referenceId = $request['message']['from']['id'];
            $this->telegramRequest->username = isset($request['message']['from']['username']) ? $request['message']['from']['username'] : '';
            $this->telegramRequest->firstName = isset($request['message']['from']['first_name']) ? $request['message']['from']['first_name'] : '';
            $this->telegramRequest->lastName = isset($request['message']['from']['last_name']) ? $request['message']['from']['last_name'] : '';
            $this->telegramRequest->message = isset($request['message']['text']) ? $request['message']['text'] : '';
        }
        elseif (isset($request['callback_query']))
        {
            $this->telegramRequest->referenceId = $request['callback_query']['from']['id'];
            $this->telegramRequest->username = isset($request['callback_query']['from']['username']) ? $request['callback_query']['from']['username'] : '';
            $this->telegramRequest->firstName = isset($request['callback_query']['from']['first_name']) ? $request['callback_query']['from']['first_name'] : '';
            $this->telegramRequest->lastName = isset($request['callback_query']['from']['last_name']) ? $request['callback_query']['from']['last_name'] : '';
            $callback_date = isset($request['callback_query']['data']) ? $request['callback_query']['data'] : null;
            if($callback_date)
            {
                if(starts_with($callback_date, 'message:'))
                {
                    $message = mb_substr($callback_date, 8, null, 'UTF-8');
                    $this->telegramRequest->message = $message;
                }
                elseif(starts_with($callback_date, 'query:'))
                {
                    $query = mb_substr($callback_date, 6, null, 'UTF-8');
                    $this->telegramRequest->query = $query;
                }
            }
        }
        elseif (isset($request['edited_message']))
        {
            // TODO: add edited message support
        }
        elseif (isset($request['inline_query']))
        {
            // TODO: add inline query support
        }
        elseif (isset($request['chosen_inline_result']))
        {
            // TODO: add chosen inline result support
        }

        // TODO: set time
    }


    private function prepareUser()
    {
        /** @var User $user */
        $user = User::find($this->telegramRequest->referenceId);
        if(!$user)
        {
            $user = new User();
            $user->reference_id = $this->telegramRequest->referenceId;
            $user->first_name = $this->telegramRequest->firstName;
            $user->last_name = $this->telegramRequest->lastName;
            $user->username = $this->telegramRequest->username;
            $user->save();
        }
        $this->user = $user;
        Auth::login($user);
    }
}
