<?php

namespace App\States;

use App\Telegram\TelegramResponse;

class AboutUsState extends State
{
    public function handleMessage()
    {
        parent::handleMessage();

        TelegramResponse::sendMessage("در باره ما...");

        return $this->changeState("StartState");
    }
}