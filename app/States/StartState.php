<?php

namespace App\States;

use App\Telegram\TelegramResponse;

class StartState extends State
{
    public function handleMessage()
    {
        parent::handleMessage();

        if($this->telegramRequest->message == "درباره ما")
        {
            return $this->changeState("AboutUsState");
        }

        TelegramResponse::sendMessage("خوش آمدید");
    }
}