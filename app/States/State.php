<?php

namespace App\States;

use App\Telegram\TelegramRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

abstract class State
{
    /** @var TelegramRequest $telegramRequest */
    protected $telegramRequest;
    /** @var User $user */
    protected $user;

    public function handleMessage()
    {

    }

    public function handleQuery()
    {

    }

    public function init(TelegramRequest $telegramRequest)
    {
        $this->telegramRequest = $telegramRequest;
        $this->user = Auth::user();
    }

    static public function handleState(TelegramRequest $telegramRequest, $stateName = null)
    {


        if(!$stateName)
        {
            /** @var User $user */
            $user = Auth::user();
            $stateName = $user->state;
        }

        $state = StateFactory::getState($stateName);
        $state->init($telegramRequest);

        if($telegramRequest->query)
        {
            $state->handleQuery();
        }
        else
        {
            $state->handleMessage();
        }
    }

    public function changeState($newState)
    {
        $this->user->setState($newState);
        $this->telegramRequest->stateChanged = true;
        $this->telegramRequest->message = null;
        $this->telegramRequest->query = null;
        State::handleState($this->telegramRequest, $newState);
    }
}