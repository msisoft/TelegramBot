<?php

namespace App\States;

class StateFactory
{
    /**
     * @param $stateName
     * @return State
     */
    static public function getState($stateName)
    {
        switch ($stateName)
        {
            case "StartState":
                return new StartState();
            case "AboutUsState":
                return new AboutUsState();
            default:
                return new StartState();
        }
    }
}