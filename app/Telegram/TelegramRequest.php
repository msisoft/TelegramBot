<?php

namespace App\Telegram;

class TelegramRequest
{
    public $message = null;
    public $query = null;
    public $referenceId;
    public $username;
    public $firstName;
    public $lastName;
    public $time;
    /** @var bool $stateChanged */
    public $stateChanged = false;
}