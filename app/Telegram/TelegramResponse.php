<?php

namespace App\Telegram;

use App\User;
use Illuminate\Support\Facades\Auth;
use Telegram\Bot\Api;

class TelegramResponse
{
    static public function sendMessage($text)
    {
        /** @var User $user */
        $user = Auth::user();

        $api = new Api();

        $parameters = [
            'chat_id' => $user->reference_id,
            'text' => $text,
        ];

        $api->sendMessage($parameters);
    }
}