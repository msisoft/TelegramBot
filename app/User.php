<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @property $reference_id
 * @property $first_name
 * @property $last_name
 * @property $username
 * @property $state
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    public $primaryKey = "reference_id";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference_id', 'first_name', 'last_name', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function setState($newState)
    {
        $this->state = $newState;
        $this->save();
    }
}
