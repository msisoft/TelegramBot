<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('webhook', function () {

    $api = new Telegram\Bot\Api();
    $api->setWebhook([
        'url'=> env('APP_URL') . '/public/TelegramBot/' . env('TG_WEBHOOK_URL_TOKEN'),
    ]);
    $this->info(env('APP_URL') . '/public/TelegramBot/' . env('TG_WEBHOOK_URL_TOKEN') . PHP_EOL . "Webhook updated successfully.");
});